// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDEhi7iyo4Q3aUazGn35DYletiPJvrE9QI",
    authDomain: "homew4-d7780.firebaseapp.com",
    databaseURL: "https://homew4-d7780.firebaseio.com",
    projectId: "homew4-d7780",
    storageBucket: "homew4-d7780.appspot.com",
    messagingSenderId: "776163641793"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
